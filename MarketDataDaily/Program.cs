﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Odbc;
using NxCoreAPI;

namespace MarketDataDaily
{
    public class Stk
    {
        public String   Ticker;
        public DateTime TradeDate;
        public Double Open;
        public Double High;
        public Double Low;
        public Double Close;
        public ulong Volume;
        public Double VWAP;
        public double OpeningAuctionVolume;
        public double ClosingAuctionVolume;
        public String ListExch;
        public String ReptExch;
        public Double TPV;
        public Double TV;
        public DateTime stime;
        public DateTime etime;
        public string td;


        public Stk(String Ticker, DateTime TradeDate)
        {
            this.Ticker = Ticker;
            this.TradeDate = TradeDate;
            this.Open = 0;
            this.High = 0;
            this.Low = double.MaxValue;
            this.Close = 0;
            this.Volume = 0;
            this.VWAP = 0;
            this.OpeningAuctionVolume = 0;
            this.ClosingAuctionVolume = 0;
            this.TPV = 0;
            this.TV = 0;
            this.td = TradeDate.ToString("yyyyMMdd");
            this.stime = new DateTime(int.Parse(this.td.Substring(0, 4)), int.Parse(this.td.Substring(4, 2)), int.Parse(this.td.Substring(6, 2)), 9, 30, 0, 0);
            this.etime = new DateTime(int.Parse(this.td.Substring(0, 4)), int.Parse(this.td.Substring(4, 2)), int.Parse(this.td.Substring(6, 2)), 16, 0, 0, 999);

        }

        public void HiLo(double P)
        {
            if (P > this.High)
            {
                this.High = P;
            }

            if (P < Low)
            {
                this.Low = P;
            }
        }

        public void Compute_VWAP(double P, double S)
        {
            this.TV += S;
            this.TPV += P * S;
            this.VWAP = this.TPV / this.TV;
        }
    };

    class Dbase
    {
        public string server;
        public string database;
        public OdbcConnection DBConn = new OdbcConnection();
        public OdbcCommand DBCmd = new OdbcCommand(); // 

        public Dbase(string server, string database)
        {
            this.server = server;
            this.database = database;
        }

        public void Connect()
        {
            this.DBConn = new OdbcConnection("DRIVER={SQL Server};Server=" + this.server + ",1433;Database=" + this.database + ";Uid=rnatarajan;Pwd=DataOneIndia234;");
            this.DBConn.Open();
            this.DBCmd = this.DBConn.CreateCommand();
        }

        public void ExecuteSQL(string sql)
        {
            this.DBCmd.CommandText = sql;
            this.DBCmd.ExecuteNonQuery();
        }

        public void Close()
        {
            DBCmd.Dispose();
            DBConn.Close();
        }
    }

    class Program
    {
        static List<string> TickerList = new List<string>();
        static Dictionary<string, Stk> TickerDict = new Dictionary<string, Stk>();
        //static string td = "20170801";
        

        //static DateTime stime = new DateTime(int.Parse(td.Substring(0, 4)), int.Parse(td.Substring(4, 2)), int.Parse(td.Substring(6, 2)), 9, 30, 0, 0);
        //static DateTime etime = new DateTime(int.Parse(td.Substring(0, 4)), int.Parse(td.Substring(4, 2)), int.Parse(td.Substring(6, 2)), 16, 0, 0, 999);


        static void Main(string[] args)
        {
            TickerList = TickerFromDB();

            DateTime sdt = new DateTime(2011, 1, 1);
            DateTime edt = new DateTime(2017, 8, 4);

            do
            {
                string day = sdt.ToString("ddd");

                if ((day == "Sun") || (day == "Sat"))
                {
                    sdt = sdt.AddDays(1);
                    continue;
                }
                string pdate = sdt.ToString("yyyyMMdd");
                  
                DateTime now = DateTime.Now;
                ProcessDate(pdate);
                DateTime now2 = DateTime.Now;

                var ts = now2.Subtract(now);
                Console.WriteLine("Processing Date : {0}, Time Taken {1} Hours {2} Mins {3} Secs ", pdate, ts.Hours, ts.Minutes, ts.Seconds);
                sdt = sdt.AddDays(1);

            } while (sdt.CompareTo(edt) < 0);

            
            
        }

        static void ProcessDate(string td)
        {
            string tapefile = "Y:\\__DATE__.GS.nx2";
            tapefile = tapefile.Replace("__DATE__", td);

            DateTime TradeDate = new DateTime(int.Parse(td.Substring(0, 4)), int.Parse(td.Substring(4, 2)), int.Parse(td.Substring(6, 2)), 0, 0, 0);

            TickerDict.Clear();

            foreach (string t in TickerList)
            {
                TickerDict[t] = new Stk(t, TradeDate);
            }

            NxCore.ProcessTape(tapefile,
                             null,
                             (uint)(NxCore.NxCF_EXCLUDE_OPRA | NxCore.NxCF_EXCLUDE_CRC_CHECK | NxCore.NxCF_EXCLUDE_QUOTES | NxCore.NxCF_EXCLUDE_QUOTES2),
                             0,
                             Program.OnNxCoreCallback);
            UpdateDB();          
        }

        static void UpdateDB()
        {
            Dbase dbase = new Dbase("10.10.1.78", "MarketData");
            dbase.Connect();

            string sql;

            foreach (string t in TickerList)
            {
                Stk tt = TickerDict[t];

                //Console.WriteLine("Sym : {0}, Open : {1}, High : {2}, Low : {3}, Close : {4}, VWAP : {5}, OAVOL : {6}, CAVOL : {7}, Volume : {8}, List Exch : , Rept Exch :  ", t, tt.Open, tt.High, tt.Low, tt.Close, tt.VWAP, tt.OpeningAuctionVolume, tt.ClosingAuctionVolume, tt.Volume);
                if (tt.Low == Double.MaxValue)
                {
                    tt.Low = 0;
                    Console.WriteLine("Issue with Sym : {0}, TradeDate : {1}, TapeDate : {2}", tt.Ticker, tt.TradeDate, tt.td);

                }
                sql = string.Format("INSERT INTO MarketData..MarketDataDaily(Ticker, TradeDate, OpenPrice, High, Low, ClosePrice, VWAP, Volume, OpeningAuctionVolume, ClosingAuctionVolume) Values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}', '{8}', '{9}')", tt.Ticker, tt.TradeDate, tt.Open, tt.High, tt.Low, tt.Close, Math.Round(tt.VWAP, 4), tt.Volume, tt.OpeningAuctionVolume, tt.ClosingAuctionVolume);
                dbase.ExecuteSQL(sql);
            }
            dbase.Close();
        }

        static unsafe int OnNxCoreCallback(IntPtr pSys, IntPtr pMsg)
        {
            // Alias structure pointers to the pointers passed in.
            NxCoreSystem* pNxCoreSys = (NxCoreSystem*)pSys;
            NxCoreMessage* pNxCoreMsg = (NxCoreMessage*)pMsg;

            // Get the symbol for category message
            String Symbol = new String(&pNxCoreMsg->coreHeader.pnxStringSymbol->String + 1);

            // Skips non-universe tickers    
            if (TickerList.IndexOf(Symbol) == -1)                   
            {
                return NxCore.NxCALLBACKRETURN_CONTINUE;
            }

            // NxDate td = pNxCoreMsg->coreHeader.nxSessionDate;
            // NxTime tt = pNxCoreMsg->coreHeader.nxExgTimestamp;

            NxDate td = pNxCoreSys->nxDate;
            NxTime tt = pNxCoreSys->nxTime;

            DateTime tradetime = new DateTime(td.Year, td.Month, td.Day, tt.Hour, tt.Minute, tt.Second, tt.Millisecond);
            DateTime stime = new DateTime(td.Year, td.Month, td.Day, 9, 30, 0);

            
            if ((tradetime.CompareTo(stime) < 0))// || (tradetime.CompareTo(etime) > 0))
            {
                return NxCore.NxCALLBACKRETURN_CONTINUE;
            }
            

            switch (pNxCoreMsg->MessageType)
            {
                case NxCore.NxMSG_TRADE:
                    //Console.WriteLine("Trade Message");
                    OnNxTradeCallback(pNxCoreSys, pNxCoreMsg);
                    break;
            }

            return NxCore.NxCALLBACKRETURN_CONTINUE;
        }

        static unsafe int OnNxTradeCallback(NxCoreSystem* pNxCoreSys, NxCoreMessage* pNxCoreMsg)
        {
            String Symbol = new String(&pNxCoreMsg->coreHeader.pnxStringSymbol->String + 1);

            // Assign a pointer to the Trade data
            NxCoreTrade* Trade = &pNxCoreMsg->coreData.Trade;

            
            if ((Trade->PriceFlags & NxCore.NxTPF_EXGCANCEL) == NxCore.NxTPF_EXGCANCEL
            || (Trade->PriceFlags & NxCore.NxTPF_EXGINSERT) == NxCore.NxTPF_EXGINSERT
            || (Trade->PriceFlags & NxCore.NxTPF_SETTLEMENT) == NxCore.NxTPF_SETTLEMENT)
                return NxCore.NxCALLBACKRETURN_CONTINUE;

            //var isNoLast = Trade->ConditionFlags & NxCore.NxTCF_NOLAST;
            //if (isNoLast == 1)
            //    return NxCore.NxCALLBACKRETURN_CONTINUE;

            // Get the price
            double Price = NxCore.PriceToDouble(Trade->Price, Trade->PriceType);
            double Size = Trade->Size;

            TickerDict[Symbol].HiLo(Price);
            TickerDict[Symbol].Compute_VWAP(Price, Size);

            // Get the exchange
            //var Reptexg = (new string(NxCore.GetDefinedString(NxCore.NxST_EXCHANGE, pNxCoreMsg->coreHeader.ReportingExg))).Split('|')[0];
            //var Listexg = (new string(NxCore.GetDefinedString(NxCore.NxST_EXCHANGE, pNxCoreMsg->coreHeader.ListedExg))).Split('|')[0];


            //* Trade->VolumeType (Incremental, Non-Incremental, Total)
            switch (Trade->TradeCondition)
            {
                case 62:
                case 7:
                case 6:                                                     // Added New
                case 5:                                                     // Added New
                    //Console.WriteLine("Inside Open Price ");
                    TickerDict[Symbol].Open = Price;
                    TickerDict[Symbol].OpeningAuctionVolume = Size;
                    break;

                case 63:
                case 51:
                case 98:
                    //Console.WriteLine("Inside Close Price ");
                    TickerDict[Symbol].Close = Price;
                    TickerDict[Symbol].ClosingAuctionVolume = Size;
                    TickerDict[Symbol].Volume = Trade->TotalVolume;
                    break;
            }

            

                return NxCore.NxCALLBACKRETURN_CONTINUE;
        }





        static List<string> TickerFromDB()
        {
            OdbcConnection DBConn = new OdbcConnection("DRIVER={SQL Server};Server=10.10.1.78,1433;Database=MarketData;Uid=rnatarajan;Pwd=DataOneIndia234;");
            DBConn.Open();

            OdbcCommand DBCmd = DBConn.CreateCommand();
            DBCmd.CommandText = "select distinct Ticker from (select Ticker from dbo.TradingList union select OldTicker as Ticker from MarketData.dbo.SymbolChanges) t";

            //var TickerList = new List<string>();
            OdbcDataReader DBReader = DBCmd.ExecuteReader();

            while (DBReader.Read())
            {
                string Ticker = (string)DBReader["Ticker"];
                TickerList.Add(Ticker);
            }
            DBReader.Close();
            DBCmd.Dispose();
            DBConn.Close();
            return TickerList;
        }
    }
}
